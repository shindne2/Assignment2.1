import json
class WikiQuery:
    def __init__(self, jsonurl):
        self.jsonfile = json.load(open(jsonurl))


    #Find how much a movie has grossed
    def movieGrossed(self, movieTitle):
        return self.jsonfile['movies'][movieTitle]['Total Gross']

    #List which movies an actor has worked in
    def moviesActorIsIn(self, actorName):
        return self.jsonfile['actors'][actorName]['movies']

    #List which actors worked in a movie
    def actorsInMovie(self, movieName):
        return self.jsonfile['movies'][movieName]['actors']

    #List the youngest X actors
    def youngestActors(self, youngestXActors):
        actors = self.jsonfile['actors'].items()
        name_age = list()
        for i in actors:
            if 'age' in i[1] and i[1]['age'] != []:
                name_age.append( (i[0],i[1]['age']))
        return sorted(name_age,key = lambda x: x[1])[:youngestXActors]


    #List the oldest X actors
    def oldestActors(self, oldestXActors):
        actors = self.jsonfile['actors'].items()
        name_age = list()
        for i in actors:
            if 'age' in i[1] and i[1]['age'] != []:
                name_age.append( (i[0],i[1]['age']))
        return sorted(name_age,key = lambda x: x[1])[-1*oldestXActors:]

    #List all the movies for a given year
    def moviesInGivenYear(self, year):
        movies = self.jsonfile['movies'].items()
        movieNames = []
        for movie in movies:
            if movie[1]['year'] == year:
                movieNames.append(movie[0])
        return movieNames

    #List all the actors for a given year
    def actorsInGivenYear(self,year):
        movies = self.jsonfile['movies'].items()
        actorNames = list()
        for movie in movies:
            if movie[1]['year'] == year:
                for actor in movie[1]['actors']:
                    if actor not in actorNames:
                        actorNames.append(actor)

        return actorNames






b = WikiQuery('movies.json')
print(b.actorsInGivenYear(2004))