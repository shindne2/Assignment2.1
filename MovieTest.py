import unittest
from Analysis import Analysis
import MovieAPI

class TestAnalysisAndAPI(unittest.TestCase):

	def testDistributionOfGross(self):
		actors = dict()
		x = Analysis('test.json')
		for i in range(50):
			actors[i] = dict()
			actors[i]['Total Gross'] = i
		
		self.assertEqual(range(50), x.distributionOfGross(actors), 'Something Isnt Right')

	def testAverageAgeOfActorsDuringMoveVsGross(self):
		movies = dict()
		for i in range(10):
			movies[i] = dict()
			movies[i]['Total Gross'] = i + 30000
			movies[i]['year'] = i + 2000
			movies[i]['actors'] = [str(k + i) for k in range(3)]

		actors = dict()

		for i in range(13):
			actors[str(i)] = dict()
			actors[str(i)]['age'] =i + 20

		x = Analysis('test.json')
		vals = x.AverageAgeOfActorsDuringMovieVsGross(actors, movies)
		print(vals)
		self.assertEqual([(5. + 2.*i) for i in range(9)],vals[0], "Average Age Isn't Right")
		self.assertEqual([(30000 + i) for i in range(1,10)], vals[1], 'Gross isnt right')




		


if __name__ == '__main__':
	unittest.main()

