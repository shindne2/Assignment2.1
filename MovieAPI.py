from flask import Flask, request
import json
import urllib

app = Flask(__name__)


def find_substring(string):
    string = string[string.index('%22') + len('%22'):]
    string = string[:string.index('%22')]
    return string



#def parse_string(string):




@app.route("/movies")
def get_movie_with_attrbs():
    movies = json.load(open('test.json'))['movies']
    attrbs = ['name', 'actors', 'Total_Gross', 'year']
    for i in attrbs:
        query = request.args.get(i)
        if i == 'name' and query != None:
            movies = {k:v for k,v in movies.items() if query.replace('_', ' ') in k}
        elif i == 'actors' and query != None:
            movies = {k:v for k,v in movies.items() if query.replace('_', ' ') in str(v[i])}
        elif i == 'Total_Gross' and query != None:
            movies = {k:v for k,v in movies.items() if v['Total Gross'] == int(query)}
        elif query != None:
            movies = {k:v for k,v in movies.items() if v[i] == int(query)}
    
    return str(movies)
    #parsed_attrs = parse_string(attrs)



@app.route("/actors")
def get_actor_with_attrbs():
    actors = json.load(open('test.json'))['actors']
    attrbs = ['name', 'movies', 'age','Total_Gross']
    for i in attrbs:
        query = request.args.get(i)
        if i == 'name' and query != None:
            actors = {k:v for k,v in actors.items() if query.replace('_', ' ') in k }
        elif i == 'movies' and query != None:
            actors = {k:v for k,v in actor.items() if query.replace('_', ' ') in str(v['movies'])}
        elif i == 'Total_Gross' and query != None:
            actors = {k:v for k,v in actors.items() if v['Total Gross'] == int(query)}
        elif query != None:
            actors = {k:v for k,v in actors.items() if v[i] == int(query)}

    return str(actors)





@app.route("/actors/<string:actorname>")
def get_actors(actorname):
    actors = json.load(open('test.json'))['actors']
    return str(actors[actorname.replace("_", " ")])
   


@app.route("/movies/<string:moviename>")
def get_movie(moviename):
    movies = json.load(open('test.json'))['movies']
    return str(movies[moviename.replace("_", " ")])


@app.route("/api/a/actors/<string:actorname>", methods = ['PUT'])
def put_actor(actorname):
    jsonfile = json.load(open('test.json'))
    actorprofile = jsonfile['actors'][actorname.encode('ascii').replace('_', ' ')]
    for k,v in request.json.items():
        if k not in ['movies', 'age','total_gross']:
            return "400 Bad Request"
        else:
            if k == 'total_gross':
                actorprofile['Total Gross'] = v
            else:
                actorprofile[k] = v

    with open('test.json', 'w') as fp:
        json.dump(jsonfile, fp, indent = 5)

            
    return "200 OK"


@app.route("/api/m/movies/<string:moviename>", methods = ['PUT'])
def put_movie(moviename):
    jsonfile = json.load(open('test.json'))
    movieprofile = jsonfile['movies'][moviename.encode('ascii').replace('_', ' ')]
    for k,v in request.json.items():
        if k not in ['movies', 'age','total_gross']:
            return "400 Bad Request"
        else:
            if k == 'box_office':
                movieprofile['total_gross'] = v
            else:
                movieprofile[k] = v

    with open('test.json', 'w') as fp:
        json.dump(jsonfile, fp, indent = 5)

            
    return "200 OK"


@app.route("/movies", methods = ['POST'])
def add_movie():
    jsonfile = json.load(open('test.json'))
    movies = jsonfile['movies']
    name = request.json['name']
    movies[name] = dict()
    for k,v in request.json.items():
       if k != 'name':
            movies[name][k] = v 

    with open('test.json', 'w') as fp:
        json.dump(jsonfile, fp, indent = 5)
    return 'OK1'


@app.route("/actors", methods = ['POST'])
def add_actor():
    jsonfile = json.load(open('test.json'))
    actors = jsonfile['actors']
    name = request.json['name']
    actors[name] = dict()
    for k,v in request.json.items():
        if k != 'name':
            actors[name][k] = v

    with open('test.json', 'w') as fp:
        json.dump(jsonfile, fp, indent = 5)

    print(request.json)
    return 'OK2'


@app.route("/actors/<string:actorname>", methods = ['DELETE'])
def delete_actor(actorname):
    jsonfile = json.load(open('test.json'))
    actors_json = jsonfile['actors']
    actor = actorname.replace('_', ' ')
    if actor in actors_json:
        del actors_json[actor]

        with open('test.json', 'w') as fp:
            json.dump(jsonfile, fp, indent = 5)
        return "200 OK"
    else:
        return "400 Bad Request"

    



@app.route("/movies/<string:moviename>", methods = ['DELETE'])
def delete_movie(moviename):
    jsonfile = json.load(open('test.json'))
    movies_json = jsonfile['movies']
    movie = moviename.replace('_', ' ')
    if movie in movies_json:
        del movie_json[actor]
        with open('test.json', 'w') as fp:
            json.dump(jsonfile, fp, indent = 5)
        return "200 OK"
    else:
        return "400 Bad Request"

    







if __name__ == "__main__":
    app.run()