import urllib
from bs4 import BeautifulSoup
import json
import time
import re
import logging

class JsonDataStore(object):

    #Initialize search by giving a wikipedia link and whtehter it is a movie or actor
    # numberofMovies is the number of movies we want to grab
    # number of Actors is the number of actors we want to grab
    def __init__(self, link, isLinkMovie,  numberOfMovies = 125, numberOfActors = 250):
        self.storage = dict()
        self.storage['actors'] = dict()
        self.storage['movies'] = dict()
        self.numberOfMovies = numberOfMovies
        self.numberOfActors = numberOfActors
        self.listOfLinks = [(link, isLinkMovie)] #used as queue for bfs
        self.openLinkAndStore() #starts search



    # Does all the heavy lifting in the bfs search
    def openLinkAndStore(self):
        while(len(self.storage['movies'].items()) < self.numberOfMovies or len(self.storage['actors']) < self.numberOfActors):
            logging.info('movies:' + str(len(self.storage['movies'].items())) + ',' + 'actors:' + str(len(self.storage['actors'])))
            if(len(self.listOfLinks) == 0):
                return

            link, isMovie = self.listOfLinks.pop()
            openurl = urllib.urlopen(link)
            if(openurl == None):
                logging.critical("CRITICAL: Cannot access link, your internet may be down!")
            webpage = BeautifulSoup(openurl.read(), 'html.parser')
            logging.info("looking at " + link)
            if(isMovie and len(self.storage['movies'].items()) <= self.numberOfMovies):
                self.scrapeMovieWiki(webpage, True, link)
            if(not isMovie and len(self.storage['actors']) <= self.numberOfActors):
                self.scrapeActorWiki(webpage, True, link)

        logging.info("Finished obtaining information just need get ages of all remaining actors")

        while((len(self.listOfLinks) != 0)):
            logging.info("number of remaining things to look in queue:" + str(len(self.listOfLinks)))
            link, isMovie = self.listOfLinks.pop()
            logging.info("looking at " + link)
            if(not isMovie):
                openurl = urllib.urlopen(link).read()
                webpage = BeautifulSoup(openurl, 'html.parser')
                self.scrapeActorWiki(webpage, False, link)


    #scrapes wikipedia movie site
    # wikipage is the beautifulsoup version of wikipedia page
    # addMoreToQueue is a boolean that dictates whether we should keep adding to the queue or stop
    # link is the url of the webpage
    def scrapeMovieWiki(self, wikipage, addMoreToQueue, link):
        movieCard = wikipage.findAll('table', {'class' : 'infobox vevent'})
        if(len(movieCard) == 1):
            title = movieCard[0].findAll('th', {'class': 'summary'})[0].text
            if title not in self.storage['movies'].items():
                starring = movieCard[0].findAll(text = 'Starring')
                boxOffice = movieCard[0].findAll(text = 'Box office')
                releaseYear = movieCard[0].findAll(text = 'Release date')
                if(len(starring) == 1 and len(boxOffice) == 1 and len(releaseYear) == 1):
                    yeartext = releaseYear[0].parent.parent.next_sibling.next_sibling.text
                    year = int(re.findall('(\d{4})', yeartext)[0])

                    listOfStars = starring[0].parent.next_sibling.parent.findAll('a')
                    self.storage['movies'][title] = dict()
                    self.storage['movies'][title]['actors'] = list()
                    self.storage['movies'][title]['year'] = year
                    for actor in listOfStars:
                        if actor.text not in self.storage['actors']:
                            self.storage['actors'][actor.text] = dict()
                            self.storage['actors'][actor.text]['movies'] = [title]
                        elif title not in self.storage['actors'][actor.text]['movies']:
                            self.storage['actors'][actor.text]['movies'].append(title)

                        self.storage['movies'][title]['actors'].append(actor.text)
                        if actor.has_attr('href') and 'wiki' in actor['href'] and addMoreToQueue:
                            self.listOfLinks.insert(0, ('https://en.wikipedia.org' + actor['href'], False))

                        gross_text = boxOffice[0].parent.next_sibling.parent.td.text
                        start = gross_text.index('$')
                        budget = self.convertToNumber(gross_text[start+1:].split())
                        self.storage['movies'][title]['Total Gross'] = budget
        else:
            logging.warning("WARNING: " + link + " could not be parsed but was accessed")




    # finds first non digit value in string
    def findFirstNonDigit(self, string):
        for i in range(len(string)):
            if not string[i].isdigit():
                return string[:i]
        return string

    #converts string to int value including the million/billion/thousand afterwards
    def convertToNumber(self, textNumber):

        number = float(self.findFirstNonDigit(textNumber[0].replace(',','')))
        if(len(textNumber) == 1):
            return int(number)
        else:
            if('billion' in textNumber[1].lower() ):
                number =  number * (10 ** 9)
            elif('million' in textNumber[1].lower() ):
                number =  number * (10 ** 6)
            elif('thousand' in textNumber[1].lower()):
                number =  number * (10 ** 3)
            else:
                logging.warning(textNumber[1] + " could not be parsed into Integer value")

            return int(number)


    #scrapes wikipedia actor site
    # wikipage is the beautifulsoup version of wikipedia page
    # addMoreToQueue is a boolean that dictates whether we should keep adding to the queue or stop
    # link is the url of the webpage
    def scrapeActorWiki(self, wikipage, addMoreToQueue, link):

        ageInfo = wikipage.findAll('span', {'class': 'noprint ForceAgeToShow'})
        actor_name = wikipage.findAll('h1')[0].text

        if(len(ageInfo) == 0):
            ageInfo = wikipage.findAll(text = 'Died')
            if(len(ageInfo) == 1):
                ageText = ageInfo[0].parent.next_sibling.next_sibling.text
                ageText = ageText[ageText.index('aged'):]
                ageText = ageText[:ageText.index(')')]
                ageInfo = int(ageText.split()[1])
            else:
                logging.warning("WARNING: " + link + " could not be parsed but was accessed")
        else:
            ageInfo = int(wikipage.findAll('span', {'class': 'noprint ForceAgeToShow'})[0].text.split()[1][:-1])

        if not addMoreToQueue:
            if actor_name not in self.storage['actors']:
                self.storage['actors'][actor_name] = dict()
            self.storage['actors'][actor_name]['age'] = ageInfo
            return


        logging.info(str(actor_name) + ":" + str(ageInfo))
        if(actor_name not in self.storage['actors']):
            self.storage['actors'][actor_name] = dict()

        self.storage['actors'][actor_name]['age'] = ageInfo

        movies= wikipage.findAll(text = 'Filmography')
        if(len(movies) != 0):
            try:
                movies = movies[1].parent.parent.next_sibling.next_sibling.next_sibling.next_sibling.findAll('a')

                for movie in movies:
                    if movie.text not in self.storage['movies'].items() and 'wiki' in movie['href'] and addMoreToQueue:
                        if movie.text != 'edit' or movie.text != 'citation needed':
                            self.listOfLinks.insert(0, ('https://en.wikipedia.org' + movie['href'], True))
                    if movie.text not in self.storage['actors'][actor_name]['movies']:
                        self.storage['actors'][actor_name]['movies'].append(movie.text)
            except:
                logging.warning("WARNING: " + link + " could not be parsed but was accessed")
                return




#saves dictionary to json
def saveDictionaryToJson(dictionary, filename):
    with open(filename,'w') as fp:
        json.dump(dictionary, fp, indent = 5)




start_time = time.time()
movieData = JsonDataStore("https://en.wikipedia.org/wiki/Glory_(1989_film)",True  )
saveDictionaryToJson(movieData.storage, 'movies1.json')
logging.info("My program took", str(time.time() - start_time), "seconds to run")
