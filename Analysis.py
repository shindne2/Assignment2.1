import json
import matplotlib.pyplot as plt


class Analysis:
    def __init__(self, jsonurl):
        openjson = open(jsonurl)
        self.jsonfile = json.load(openjson)
        self.hubActors()
        self.AgeVsGross()
        self.AverageAgeOfActorsDuringMovieVsGross(self.jsonfile['actors'], self.jsonfile['movies'])


    '''
    Is there an age group that generates the most amount of money? 
    What does the correlation between age and grossing value look like?
    '''

    def distributionOfGross(self, actors):
        total_gross = []
        for actor in actors:
            total_gross.append(actors[actor]['Total Gross'])

        return sorted(total_gross)



    def AverageAgeOfActorsDuringMovieVsGross(self, actors , movies):
        nonOutlierMovies = {k: v for k, v in movies.items() if v['Total Gross'] > 30000 and v['year'] > 1900}
        average_age = []
        gross = []
        for k,v in nonOutlierMovies.items():
            movie_year = v['year']
            total_age = 0
            numb_actors = 0
            for actor in v['actors']:
                if actor in actors:
                    numb_actors += 1
                    total_age += (v['year'] - 2018 + actors[actor]['age'])
            if numb_actors > 0:
                average_age.append(float(total_age)/(float(numb_actors)))
                gross.append(v['Total Gross'])
            #else:
                #print(k)
        plt.plot(average_age, gross, 'bo')
        plt.title('Average Age of Actors in Movie vs Total_Gross')
        plt.show()
        return average_age, gross



    def AgeVsGross(self):
        actors = self.jsonfile['actors']
        nonOutlierActors = {k: v for k, v in actors.items() if v['Total Gross'] > 1000000}

        avg_gross = []
        age = []

        for k,v in nonOutlierActors.items() :
            v['Avg Gross'] = v['Total Gross']/len(v['movies'])
            avg_gross.append(v['Avg Gross'])
            age.append(v['age'])
            #print(k, v['Avg Gross'], v['age'])

        plt.plot(age, avg_gross, 'bo')
        plt.title('Age vs Average Gross of Movies')
        plt.show()
        


    def hubActors(self, topXActors= 5):
        actors = self.jsonfile['actors']
        movies = self.jsonfile['movies']
        common_actors = dict()

        for actor, info in actors.items():
            if actor not in common_actors:
                common_actors[actor] = list()
                actors_movies = info['movies']
                for movie in actors_movies:
                    if movie in movies:
                        connected_to_actors = movies[movie]['actors']
                        for one_off_actor in connected_to_actors:
                            if one_off_actor not in common_actors[actor]:
                                common_actors[actor].append(one_off_actor)

        top_actors = sorted(common_actors, key=lambda k: len(common_actors[k]), reverse=True)[:topXActors]
        top_actors_values = [len(common_actors[i]) for i in top_actors]

        #print(type(top_actors))
        #print(type(tuple(top_actors_values)))
        self.plotBarGraph(tuple(top_actors), top_actors_values, "# of actors connected to", "Most Connected Actors")

    def plotBarGraph(self, objects, yvals, ylabel, title):

        y_pos = range(len(objects))

        plt.bar(y_pos, yvals, align='center')
        plt.xticks(y_pos, objects)
        plt.ylabel(ylabel)
        plt.title(title)
        plt.show()


movies = Analysis('test.json')




