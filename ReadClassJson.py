import json

class ReadClassJson:

    def __init__(self, testjsonurl):
        self.jsonfile = json.load(open(testjsonurl))
        self.myDictGraph = dict()
        self.myDictGraph['actors'] = dict()
        self.myDictGraph['movies'] = dict()
        for jsonpart in self.jsonfile:
            self.parse(jsonpart)




    def parse(self, json):

        for name, info in json.items():
            if info['json_class'] == 'Actor':
                actors = self.myDictGraph['actors']
                actors[name] = dict()
                actordict =actors[name]
                actordict['age'] = info['age']
                actordict['movies'] = list(info['movies'])
                actordict['Total Gross'] = info['total_gross']

            elif info['json_class'] == 'Movie':
                movies = self.myDictGraph['movies']
                movies[name] = dict()
                moviedict = movies[name]
                moviedict['actors'] = list(info['actors'])
                moviedict['Total Gross'] = info['box_office']
                moviedict['year'] = info['year']


    def saveDictionaryToJson(self, filename):
        with open(filename, 'w') as fp:
            json.dump(self.myDictGraph, fp, indent=5)







val = ReadClassJson('data.json')
val.saveDictionaryToJson('test.json')